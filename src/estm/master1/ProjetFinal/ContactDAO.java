package estm.master1.ProjetFinal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
 

public class ContactDAO {

    private Connection conn;
    private Statement stmt;
    private PreparedStatement prepStmt;

    public ContactDAO() {}

    // Insert new contact in DB and return ID of new inserted Contact
	public int addContact(Contact newContact) {
        int createdContactID = 0;
        try {
            conn = ConnectionFactory.getConnection();
            prepStmt = conn.prepareStatement("INSERT INTO contacts (Nom, Prenom, NumTel, Email, Puce, Adresse, Type) VALUES (?, ?, ?, ?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);

            prepStmt.setString(1, newContact.getNom());
            prepStmt.setString(2, newContact.getPrenom());
            prepStmt.setString(3, newContact.getNumTel());
            prepStmt.setString(4, newContact.getEmail());
            prepStmt.setInt(5, newContact.getPuce());
            prepStmt.setString(6, newContact.getAdresse());
            prepStmt.setString(7, newContact.getType());

            prepStmt.executeUpdate();
            ResultSet rs = prepStmt.getGeneratedKeys();
            rs.next();
            createdContactID = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(prepStmt);
            DBUtils.close(conn);
        }

        return createdContactID;
	}
	

	public int deleteContact(int id) {
        int res = 0;
		try {
            conn = ConnectionFactory.getConnection();
            PreparedStatement prepStmt = conn.prepareStatement("DELETE FROM contacts WHERE Id=?");
            prepStmt.setInt(1, id);
            res = prepStmt.executeUpdate();
        } catch (SQLException e) {
		    e.printStackTrace();
        } finally {
            DBUtils.close(prepStmt);
            DBUtils.close(conn);
        }
		return res;	
	}


	public int updateContact(Contact updatedContact) {
        int res = 0;
        try {
            conn = ConnectionFactory.getConnection();
            int id = updatedContact.getId();

            prepStmt = conn.prepareStatement("UPDATE contacts SET Nom = ?, Prenom = ?, NumTel = ?, Email = ?, Puce = ?, Adresse = ? WHERE Id = ?");
            prepStmt.setString(1, updatedContact.getNom());
            prepStmt.setString(2, updatedContact.getPrenom());
            prepStmt.setString(3, updatedContact.getNumTel());
            prepStmt.setString(4, updatedContact.getEmail());
            prepStmt.setInt(5, updatedContact.getPuce());
            prepStmt.setString(6, updatedContact.getAdresse());
            prepStmt.setInt(7, id);

            res = prepStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(prepStmt);
            DBUtils.close(conn);
        }

		return res;
	}
	
	public Contact findContact(int id) {
        Contact searchResult = new Contact();

        try {
            conn = ConnectionFactory.getConnection();

            prepStmt = conn.prepareStatement("SELECT * FROM contacts WHERE id=?");
            prepStmt.setInt(1, id);

            ResultSet res = prepStmt.executeQuery();
            res.next();

            int id1 = res.getInt("id");
            String nom = res.getString("Nom");
            String prenom = res.getString("Prenom");
            String numTel = res.getString("NumTel");
            int puce = res.getInt("Puce");
            String email = res.getString("Email");
            String adresse = res.getString("Adresse");
            String type = res.getString("Type");

            searchResult.setId(id1);
            searchResult.setNom(nom);
            searchResult.setPrenom(prenom);
            searchResult.setNumTel(numTel);
            searchResult.setAdresse(adresse);
            searchResult.setPuce(puce);
            searchResult.setEmail(email);
            searchResult.setType(type);
        } catch (SQLException e) {
            e.getMessage();
        } finally {
            DBUtils.close(prepStmt);
            DBUtils.close(conn);
        }
		
		return searchResult;
	}

	
	public ArrayList<Contact> allContact(){
		ArrayList<Contact> allContacts = new ArrayList();

		try {
            conn = ConnectionFactory.getConnection();
			stmt = conn.createStatement();
			ResultSet res = stmt.executeQuery("SELECT * FROM contacts");

			while (res.next()) {
				int id = res.getInt("Id");
				String nom = res.getString("Nom");
				String prenom = res.getString("Prenom");
				String numTel = res.getString("NumTel");
				int puce = res.getInt("Puce");
				String email = res.getString("Email");
				String adresse = res.getString("Adresse");
				String type = res.getString("type");
				
				Contact buffContact = new Contact(id, prenom, nom, numTel, email, adresse, puce, type);
				
				allContacts.add(buffContact);
			}
		} catch (SQLException e) {
            System.out.println(e.getMessage());
		} finally {
		    DBUtils.close(stmt);
		    DBUtils.close(conn);
        }

		return allContacts;
	}

    private void convertToCsv(ResultSet rs, String fileName) throws SQLException, FileNotFoundException {
        PrintWriter csvWriter = new PrintWriter(new File("exports/"+fileName+".csv")) ;
        ResultSetMetaData meta = rs.getMetaData() ;
        int numberOfColumns = meta.getColumnCount() ;
        String dataHeaders = "\"" + meta.getColumnName(1) + "\"" ;
        for (int i = 2 ; i < numberOfColumns + 1 ; i ++ ) {
            dataHeaders += ",\"" + meta.getColumnName(i).replaceAll("\"","\\\"") + "\"" ;
        }
        csvWriter.println(dataHeaders) ;
        while (rs.next()) {
            String row = "\"" + rs.getString(1).replaceAll("\"","\\\"") + "\""  ;
            for (int i = 2 ; i < numberOfColumns + 1 ; i ++ ) {
                row += ",\"" + rs.getString(i).replaceAll("\"","\\\"") + "\"" ;
            }
            csvWriter.println(row) ;
        }
        csvWriter.close();
    }
}
