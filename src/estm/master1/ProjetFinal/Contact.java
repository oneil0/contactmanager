package estm.master1.ProjetFinal;

public class Contact extends ContactDAO {
	private int Id;
	private String prenom;
	private String nom;
	private String numTel;
	private String email;
	private String adresse;
	private int puce;
	private String type;

	public Contact(){}
	
	public Contact(String prenom, String nom, String numTel, String email, String adresse, int puce, String type) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.numTel = numTel;
		this.email = email;
		this.adresse = adresse;
		this.puce = puce;
		this.type = type;
	}

	public Contact(int id, String prenom, String nom, String numTel, String email, String adresse, int puce, String type) {
		super();
		Id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.numTel = numTel;
		this.email = email;
		this.adresse = adresse;
		this.puce = puce;
		this.type = type;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getPuce() {
		return puce;
	}

	public void setPuce(int puce) {
		this.puce = puce;
	}
	
	public String getType()
	{
		return type;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public void afficher() 
	{
		System.out.println("Id: "+Id+"\tPrenom: "+prenom+"\tNom: "+nom+"\tNumero: "+numTel+"\tEmail: "+email+"\tAdresse: "+adresse+"\tPuce: "+puce+"\tType: "+type);
	}
	
	
}
