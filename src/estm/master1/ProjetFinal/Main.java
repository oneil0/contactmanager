    package estm.master1.ProjetFinal;

    import java.util.ArrayList;
    import java.util.Scanner;
    import java.util.regex.Matcher;
    import java.util.regex.Pattern;

    public class Main {
        private static Pattern pattern;
        private static Matcher matcher;

        public static boolean  valideNumeTel(String numTel) {
            String numTelPattern = "^\\+[0-9]{12}";
            pattern = Pattern.compile(numTelPattern);
            matcher = pattern.matcher(numTel);

            boolean res = matcher.matches();
            return res;
        }

        public static boolean valideEmail(String email) {
            String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

            pattern = Pattern.compile(emailPattern);
            matcher = pattern.matcher(email);

            boolean res = matcher.matches();
            return res;
        }

        public static boolean validerType(String type) {
            String type1 = "Parent";
            String type2 = "Ami";
            String type3 = "Simple";

            pattern = Pattern.compile(type1);
            pattern = Pattern.compile(type2);
            pattern = Pattern.compile(type3);

            matcher = pattern.matcher(type);

            boolean test = matcher.matches();
            return test;
        }

        public static void main(String[] args) {

            ContactDAO cnt = new ContactDAO();

            ArrayList<Contact> allContacts = cnt.allContact();

//            cnt.findContact(6).afficher();
//
//            Contact nContact = new Contact(6,"Ibrahima", "Niebe", "+221778889966", "iboula@gmail.com", "parcelle", 2, "parent");
//            cnt.updateContact(nContact);
//            cnt.findContact(6).afficher();
//            //            nContact.afficher();
////            System.out.println(cnt.addContact(nContact));

            for (int i = 0; i < cnt.allContact().size(); i++) {
                allContacts.get(i).afficher();
            }
        }
    }
